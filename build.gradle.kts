import dao4u.aw.gradle.dependencies.Dependency

buildscript {
    repositories {
        google()
        mavenCentral()
        maven("https://plugins.gradle.org/m2/")
    }
    dependencies {
        classpath(dao4u.aw.gradle.dependencies.Gradle.android)
        classpath(dao4u.aw.gradle.dependencies.Gradle.kotlin)
        classpath(dao4u.aw.gradle.dependencies.Gradle.hilt)
        classpath(dao4u.aw.gradle.dependencies.Gradle.jacoco)
        classpath(dao4u.aw.gradle.dependencies.Gradle.playService)
        classpath(dao4u.aw.gradle.dependencies.Gradle.ksp)
        classpath(dao4u.aw.gradle.dependencies.Gradle.appDistribution)
        classpath(dao4u.aw.gradle.dependencies.Gradle.ktlint)
    }
}

allprojects {
    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions.jvmTarget = JavaVersion.VERSION_1_8.toString()

        kotlinOptions.allWarningsAsErrors = false

        kotlinOptions.freeCompilerArgs += "-Xopt-in=kotlin.RequiresOptIn"
        kotlinOptions.freeCompilerArgs += "-Xopt-in=kotlin.Experimental"
    }

    val composeRegex: Regex = "androidx.compose.\\w+".toRegex()
    val navigationRegex: Regex = "androidx.navigation".toRegex()
    configurations.all {
        resolutionStrategy.eachDependency {
            if (this.requested.group.contains(composeRegex)) {
                this.useVersion(Dependency.Compose.version)
            } else if (this.requested.group.contains(navigationRegex)) {
                this.useVersion(Dependency.Compose.navVersion)
            }
        }
    }

    apply(plugin = "org.jlleitschuh.gradle.ktlint")
}

task("clean", Delete::class) {
    delete(rootProject.buildDir)
}