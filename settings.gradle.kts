dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        maven("https://plugins.gradle.org/m2/")
    }
}

rootProject.name = "dao4u"
enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

include(":app")
include(":network", ":network-api")

val modules = hashMapOf<String, String>()

rootProject.projectDir.listFiles()
    .forEach {
        findSubProjects(it)
    }

fun findSubProjects(
    file: File
) {
    if (file.name.startsWith(".")) {
        return
    }

    if (file.name == "build.gradle" || file.name == "build.gradle.kts") {
        modules[file.parentFile.name] = file.parentFile.path
        return
    }

    if (file.isDirectory) {
        file.listFiles().forEach {
            findSubProjects(it)
        }
    }
}

for (project in rootProject.children) {

    if (modules.containsKey(project.name)) {
        val directory = modules[project.name]
        project.projectDir = File(directory)
    }
}