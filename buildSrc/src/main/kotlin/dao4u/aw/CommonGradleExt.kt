import dao4u.aw.gradle.dependencies.Dependency
import dao4u.aw.gradle.locals.LocalProjects
import com.android.build.gradle.BaseExtension
import com.android.build.gradle.LibraryExtension
import org.gradle.api.Action
import org.gradle.api.JavaVersion
import org.gradle.api.Project
import org.gradle.kotlin.dsl.DependencyHandlerScope
import org.gradle.kotlin.dsl.project
import org.jetbrains.kotlin.gradle.dsl.KotlinJvmOptions
import java.io.File
import java.util.HashMap
import kotlin.reflect.KClass
import kotlin.reflect.full.declaredMembers

fun buildLocalProjects(kClass: KClass<out Any>): HashMap<String, String> {
    val locals = hashMapOf<String, String>()

    for (member in kClass.declaredMembers) {
        locals[member.name] = member.call() as String
    }

    return locals
}

fun DependencyHandlerScope.appLocalProjectDependencies() {
    val localProjects = buildLocalProjects(LocalProjects.Local::class)
    for ((_, projectName) in localProjects) {
        "implementation"(project(projectName))
    }
}

fun DependencyHandlerScope.hiltCommon() {
    "implementation"(Dependency.Hilt.hilt)
    "kapt"(Dependency.Hilt.hiltCompiler)
}

fun BaseExtension.kspSourceSets() {
    buildTypes {
        getByName("debug") {
            sourceSets {
                getByName("main") {
                    java.srcDir(File("build/generated/ksp/debug/kotlin"))
                }
            }
        }
        getByName("release") {
            sourceSets {
                getByName("main") {
                    java.srcDir(File("build/generated/ksp/release/kotlin"))
                }
            }

        }
    }
}

fun Project.composeForLib(): Unit =
    (this as org.gradle.api.plugins.ExtensionAware).extensions.configure("android", Action<LibraryExtension> {
        buildFeatures {
            compose = true
        }
        composeOptions {
            kotlinCompilerExtensionVersion = Dependency.Compose.version
        }
    })

fun Project.composeForApp(): Unit =
    (this as org.gradle.api.plugins.ExtensionAware).extensions.configure(
        "android",
        Action<com.android.build.gradle.internal.dsl.BaseAppModuleExtension> {
            buildFeatures {
                compose = true
            }
            composeOptions {
                kotlinCompilerExtensionVersion = Dependency.Compose.version
            }
        })

fun DependencyHandlerScope.composeDep() {
    "implementation"(Dependency.Compose.ui)
    "implementation"(Dependency.Compose.foundation)
    "implementation"(Dependency.Compose.material)
    "implementation"(Dependency.Compose.uiTooling)
    "implementation"(Dependency.Compose.runtime)
    "implementation"(Dependency.Compose.liveData)

}

fun BaseExtension.commonAndroidConfig(project: Project) {
    compileSdkVersion(Dependency.Base.compileVersion)
    buildToolsVersion(Dependency.Base.buildToolVersion)

    defaultConfig {
        minSdk = Dependency.Base.minSdk
        targetSdk = Dependency.Base.targetSdk
        vectorDrawables.useSupportLibrary = true

        buildConfigField("String", "DEFAULT_HOST", "\"dev.dao4u\"")
        buildConfigField("String", "DEFAULT_GRPC_HOST", "\"dev.dao4u\"")
        buildConfigField("boolean", "DEFAULT_TLS", "true")
    }

    buildTypes {
        getByName("debug") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
            buildConfigField("boolean", "DEBUG_BUILD", "true")
        }

        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
            buildConfigField("boolean", "DEBUG_BUILD", "false")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    testOptions {
        unitTests.all {
            it.reports.html.outputLocation.set(File("${project.rootDir}/build/reports/test/${project.name}"))
            it.reports.html.required.set(true)
            it.reports.junitXml.outputLocation.set(File("${project.rootDir}/build/reports/test/${project.name}"))
            it.reports.junitXml.required.set(true)
        }
    }

    lintOptions {
        lintConfig = File("${project.rootDir}/config/lint/lint_rule.xml")
        xmlOutput = File("${project.rootDir}/build/reports/lint/${project.name}/lint-results.xml")
        htmlOutput = File("${project.rootDir}/build/reports/lint/${project.name}/lint-results.html")
    }
}

fun KotlinJvmOptions.commonKotlinOptions() {
    jvmTarget = "1.8"
}
