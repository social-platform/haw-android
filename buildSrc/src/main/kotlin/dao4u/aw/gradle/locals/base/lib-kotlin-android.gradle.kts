import dao4u.aw.gradle.dependencies.Dependency

plugins {
    id("com.android.library")
    id("code-quality")
    id("lib-tasks")
    kotlin("android")
}

android {
    commonAndroidConfig(project)

    kotlinOptions {
        commonKotlinOptions()
    }
}

dependencies {

    Dependency.Kotlin.run {
        implementation(stdLib)
        implementation(stdLibJdk8)
    }

    Dependency.AndroidTest.run {
        testImplementation(junit4)
        testImplementation(powerMock)
        testImplementation(powerMockJunit4)
        testImplementation(powerMockForMockito)
        testImplementation(junit5)
        testImplementation(mockito)
        testImplementation(mockitoKotlin)
        testRuntimeOnly(engine)
        testRuntimeOnly(vintage)
    }
}