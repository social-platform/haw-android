plugins {
    id("com.android.library")
    id("com.google.devtools.ksp")
    kotlin("android")
}

android {
    kspSourceSets()
}

ksp {
    arg("moduleName", project.name)
}
