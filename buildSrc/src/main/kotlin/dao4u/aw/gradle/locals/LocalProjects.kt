package dao4u.aw.gradle.locals

object LocalProjects {

    object Local {
        const val network = ":network"
        const val networkApi = ":network-api"
    }
}