@file:Suppress("UnstableApiUsage")

import dao4u.aw.gradle.dependencies.Dependency

plugins {
    id("com.android.library")
    kotlin("android")
}

android {
    testOptions {
        unitTests {
            isIncludeAndroidResources = true
        }
    }
}


dependencies {
    Dependency.AndroidTest.run {
        testImplementation(androidxCore)
        testImplementation(androidxRunner)
        testImplementation(androidxJunit)
        testImplementation(robolectric)
    }
}