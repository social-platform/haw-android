plugins {
    id("lib-kotlin-android")
    id("dagger.hilt.android.plugin")
    kotlin("android")
    kotlin("kapt")
}

dependencies {
    hiltCommon()
}