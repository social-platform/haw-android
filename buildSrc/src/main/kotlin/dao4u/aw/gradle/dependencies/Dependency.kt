package dao4u.aw.gradle.dependencies

object Dependency {

    object Base {
        const val buildToolVersion = "31.0.0"
        const val compileVersion = 31
        const val minSdk = 21
        const val targetSdk = 30
    }

    object Version {
        const val major = "1"
        const val minor = "0"
        const val hotfix = "0"
    }

    object Kotlin {
        const val version: String = "1.6.10"

        const val stdLib = "org.jetbrains.kotlin:kotlin-stdlib:$version"
        const val stdLibJdk8 = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:$version"
    }

    object Coroutines {
        private const val version = "1.5.0"
        const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$version"
        const val rx3 = "org.jetbrains.kotlinx:kotlinx-coroutines-rx3:$version"

        const val test = "org.jetbrains.kotlinx:kotlinx-coroutines-test:$version"
        private const val turbineVersion = "0.6.1"
        const val turbine = "app.cash.turbine:turbine:$turbineVersion"
    }

    object AndroidX {
        // or jetpack compose
        const val navigationVersion = "2.3.5"
        const val lifecycleVersion = "2.3.1"
        const val archCoreVersion = "2.1.0"

        const val core = "androidx.core:core:1.3.2"
        const val coreKtx = "androidx.core:core-ktx:1.3.2"
        const val appCompat = "androidx.appcompat:appcompat:1.3.1"
        const val activity = "androidx.activity:activity:1.2.2"

        const val navigationUiKtx = "androidx.navigation:navigation-ui-ktx:$navigationVersion"
        const val liveDataCore = "androidx.lifecycle:lifecycle-livedata-core:$lifecycleVersion"
        const val liveDataKtx = "androidx.lifecycle:lifecycle-livedata-ktx:$lifecycleVersion"
        const val viewModel = "androidx.lifecycle:lifecycle-viewmodel:$lifecycleVersion"
        const val multiDex = "androidx.multidex:multidex:2.0.1"
    }

    object Compose {
        const val version = "1.2.0-alpha07"
        private const val activityVersion = "1.6.0-alpha01"
        private const val viewModelVersion = "2.5.0-alpha06"
        private const val constraintVersion = "1.0.0"

        const val navVersion = "2.5.0-alpha04"

        const val ui = "androidx.compose.ui:ui:$version"
        const val uiTooling = "androidx.compose.ui:ui-tooling:$version"
        const val foundation = "androidx.compose.foundation:foundation:$version"
        const val runtime = "androidx.compose.runtime:runtime:$version"
        const val liveData = "androidx.compose.runtime:runtime-livedata:$version"
        const val material = "androidx.compose.material:material:$version"
        const val materialIcon = "androidx.compose.material:material-icons-core:$version"
        const val materialRipple = "androidx.compose.material:material-ripple:$version"
        const val materialIconExt = "androidx.compose.material:material-icons-extended:$version"
        const val constraintLayout =
            "androidx.constraintlayout:constraintlayout-compose:$constraintVersion"
        const val navigation = "androidx.navigation:navigation-compose:$navVersion"

        const val activity = "androidx.activity:activity-compose:$activityVersion"
        const val viewModel = "androidx.lifecycle:lifecycle-viewmodel-compose:$viewModelVersion"

    }

    object Accompanist {
        const val version = "0.24.6-alpha"

        const val swipeRefresh = "com.google.accompanist:accompanist-swiperefresh:$version"
        const val placeholder = "com.google.accompanist:accompanist-placeholder:$version"

    }

    object ComposeTest {
        const val ui = "androidx.compose.ui:ui-test-junit4:1.2.0-alpha07"
    }

    object Google {
        const val playServiceVersion = "19.0.0"

        const val material = "com.google.android.material:material:1.4.0"
        const val auth = "com.google.android.gms:play-services-auth:$playServiceVersion"
        const val basement = "com.google.android.gms:play-services-basement:17.5.0"
    }

    object Facebook {
        const val sdk = "com.facebook.android:facebook-android-sdk:[5,6)"
    }

    object AndroidTest {
        private const val junit5Version = "5.7.0"

        private const val androidXTestVersion = "1.3.0"

        const val androidxCore = "androidx.test:core:$androidXTestVersion"
        const val androidxRunner = "androidx.test:runner:$androidXTestVersion"
        const val androidxJunit = "androidx.test.ext:junit:1.1.2"
        const val espresso = "androidx.test.espresso:espresso-core:3.3.0"
        const val archCore = "androidx.arch.core:core-testing:${AndroidX.archCoreVersion}"

        const val mockito = "org.mockito:mockito-core:2.23.4"
        const val mockitoKotlin = "com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0"

        private const val powerMockVersion = "2.0.9"

        const val junit4 = "junit:junit:4.13"
        const val powerMock = "org.powermock:powermock-core:$powerMockVersion"
        const val powerMockJunit4 = "org.powermock:powermock-module-junit4:$powerMockVersion"
        const val powerMockForMockito = "org.powermock:powermock-api-mockito2:$powerMockVersion"

        const val engine = "org.junit.jupiter:junit-jupiter-engine:$junit5Version"
        const val vintage = "org.junit.vintage:junit-vintage-engine:$junit5Version"
        const val junit5 = "org.junit.jupiter:junit-jupiter-api:$junit5Version"
        const val params = "org.junit.jupiter:junit-jupiter-params:$junit5Version"

        private const val robolectricVersion = "4.5.1"
        const val robolectric = "org.robolectric:robolectric:$robolectricVersion"
        const val robolectricShadowApi = "org.robolectric:shadowapi:$robolectricVersion"

    }

    object Hilt {
        private const val version = "2.41"

        const val hilt = "com.google.dagger:hilt-android:$version"
        const val hiltCompiler = "com.google.dagger:hilt-android-compiler:$version"
    }

    object RxJava {
        const val version = "3.1.4"
        const val androidVersion = "3.0.0"

        const val base = "io.reactivex.rxjava3:rxjava:$version"
        const val android = "io.reactivex.rxjava3:rxandroid:$androidVersion"
    }

    object Retrofit {
        const val okhttpVersion = "4.9.0"
        const val retrofitVersion = "2.9.0"

        const val retrofit = "com.squareup.retrofit2:retrofit:$retrofitVersion"
        const val okhttp = "com.squareup.okhttp3:okhttp:$okhttpVersion"

        const val jacksonConverter = "com.squareup.retrofit2:converter-jackson:$retrofitVersion"
        const val rxjava3Adapter = "com.squareup.retrofit2:adapter-rxjava3:$retrofitVersion"
    }

    object Room {
        private const val roomVersion = "2.3.0"

        const val room = "androidx.room:room-ktx:$roomVersion"
        const val roomRuntime = "androidx.room:room-runtime:$roomVersion"
        const val roomCompiler = "androidx.room:room-compiler:$roomVersion"
    }

    object Timber {
        const val api = "com.jakewharton.timber:timber:5.0.1"
    }

    object Stetho {
        const val stetho = "com.facebook.stetho:stetho:1.5.1"
        const val okhttp3 = "com.facebook.stetho:stetho-okhttp3:1.5.1"
    }

    object Firebase {
        const val version = "28.0.1"
        const val crashlyticsVersion = "2.6.1"

        const val firebaseBom = "com.google.firebase:firebase-bom:$version"
        const val messaging = "com.google.firebase:firebase-messaging-ktx"
        const val analytics = "com.google.firebase:firebase-analytics-ktx"
        const val crashlytics = "com.google.firebase:firebase-crashlytics-ktx"
        const val firebaseConfig = "com.google.firebase:firebase-config-ktx"
    }

    object Jackson {
        const val kotlinModule = "com.fasterxml.jackson.module:jackson-module-kotlin:2.13.2"
    }

    object Image {
        private const val version = "2.0.0-rc02"
        const val coilCompose = "io.coil-kt:coil-compose:$version"
    }

    object Ksp {
        const val kspVersion = "1.6.10-1.0.4"
    }

    object Gson {
        const val gson = "com.google.code.gson:gson:2.8.7"
    }
}