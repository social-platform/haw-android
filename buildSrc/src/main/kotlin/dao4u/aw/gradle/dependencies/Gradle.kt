package dao4u.aw.gradle.dependencies

object Gradle {

    const val androidVersion = "7.1.2"
    const val jacocoVersion = "0.8.7"
    const val android = "com.android.tools.build:gradle:$androidVersion"
    const val kotlin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Dependency.Kotlin.version}"
    const val jacoco = "org.jacoco:org.jacoco.core:$jacocoVersion"
    const val playService = "com.google.gms:google-services:4.3.4"
    const val ksp = "com.google.devtools.ksp:com.google.devtools.ksp.gradle.plugin:${Dependency.Ksp.kspVersion}"
    const val appDistribution = "com.google.firebase:firebase-appdistribution-gradle:2.1.1"
    const val hilt = "com.google.dagger:hilt-android-gradle-plugin:2.38.1"
    const val ktlint = "org.jlleitschuh.gradle:ktlint-gradle:10.2.0"
    const val crashlytics = "com.google.firebase:firebase-crashlytics-gradle:${Dependency.Firebase.crashlyticsVersion}"
}