import dao4u.aw.gradle.dependencies.Dependency
import dao4u.aw.gradle.dependencies.Dependency.Version.hotfix
import dao4u.aw.gradle.dependencies.Dependency.Version.major
import dao4u.aw.gradle.dependencies.Dependency.Version.minor

plugins {
    id("com.android.application")

    kotlin("android")
    kotlin("kapt")
    id("dagger.hilt.android.plugin")
    id("com.google.devtools.ksp")
}

composeForApp()

android {
    commonAndroidConfig(project)

    defaultConfig {
        applicationId = "dao4u.aw"
        versionCode = System.getenv("BITBUCKET_BUILD_NUMBER")?.toInt() ?: 1
        versionName = "$major.$minor.$hotfix"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        multiDexEnabled = true
    }
    signingConfigs {
        getByName("debug") {
            storeFile = file("$rootDir/config/keystore/debug.keystore")
            storePassword = System.getenv("DEBUG_STORE_PASSWORD") ?: "android"
            keyAlias = System.getenv("DEBUG_KEY_ALIAS") ?: "androiddebugkey"
            keyPassword = System.getenv("DEBUG_KEY_PASSWORD") ?: "android"
        }

        create("release") {
            storeFile = file("$rootDir/config/keystore/release.keystore")
            storePassword = System.getenv("RELEASE_STORE_PASSWORD") ?: System.getenv("RELEASE_STORE_PASSWORD")
            keyAlias = System.getenv("RELEASE_KEY_ALIAS") ?: System.getProperty("RELEASE_KEY_ALIAS")
            keyPassword = System.getenv("RELEASE_KEY_PASSWORD") ?: System.getProperty("RELEASE_KEY_PASSWORD")
        }
    }

    buildTypes {
        getByName("debug") {
            isDebuggable = true
            signingConfig = signingConfigs.getByName("debug")
        }

        getByName("release") {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            signingConfig = signingConfigs.getByName("release")
        }
    }

    kotlinOptions {
        commonKotlinOptions()
    }
    namespace = "dao4u.aw"
}

dependencies {
    Dependency.Kotlin.run {
        implementation(stdLib)
        implementation(stdLibJdk8)
    }

    Dependency.AndroidX.run {
        implementation(appCompat)
        implementation(activity)
        implementation(liveDataKtx)
        implementation(multiDex)
    }

    implementation(Dependency.Google.material)

    Dependency.RxJava.run {
        implementation(base)
        implementation(android)
    }

    implementation(Dependency.Retrofit.okhttp)
    implementation(Dependency.Coroutines.rx3)
    implementation(Dependency.Compose.navigation)
    implementation(Dependency.Compose.navigation)

    hiltCommon()
    composeDep()

    implementation(Dependency.Compose.activity)
    implementation(Dependency.Compose.viewModel)

    Dependency.Room.run {
        implementation(roomRuntime)
        implementation(room)
        kapt(roomCompiler)
    }

    appLocalProjectDependencies()

    Dependency.AndroidTest.run {
        testImplementation(junit4)
        testImplementation(powerMock)
        testImplementation(powerMockJunit4)
        testImplementation(powerMockForMockito)
        testImplementation(junit5)
        testImplementation(mockito)
        testImplementation(mockitoKotlin)
        testRuntimeOnly(engine)
        testRuntimeOnly(vintage)
    }

    implementation(Dependency.Image.coilCompose)
}