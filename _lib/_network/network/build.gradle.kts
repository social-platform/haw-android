import dao4u.aw.gradle.dependencies.Dependency
import dao4u.aw.gradle.locals.LocalProjects.Local

plugins {
    id("lib-kotlin-hilt")
}

dependencies {
    implementation(project(Local.networkApi))
    implementation(Dependency.Retrofit.retrofit)
    implementation(Dependency.Retrofit.jacksonConverter)
    implementation(Dependency.Retrofit.rxjava3Adapter)
    implementation(Dependency.Jackson.kotlinModule)
    implementation(Dependency.RxJava.android)
    implementation(Dependency.RxJava.base)
}