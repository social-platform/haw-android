package dao4u.aw.lib.network

import com.fasterxml.jackson.databind.ObjectMapper
import dao4u.aw.lib.network.api.RetrofitApiFactory
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory

class RetrofitFactoryImpl(private val objectMapper: ObjectMapper) : RetrofitApiFactory {

    override fun <T> createApi(api: Class<T>): T =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(JacksonConverterFactory.create(objectMapper))
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .build()
            .create(api)

    companion object {
        private const val BASE_URL: String = ""
    }

}