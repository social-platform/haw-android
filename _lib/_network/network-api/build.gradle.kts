import dao4u.aw.gradle.dependencies.Dependency

plugins {
    id("lib-kotlin-android")
}

dependencies {
    implementation(Dependency.RxJava.base)
    implementation(Dependency.RxJava.android)
}