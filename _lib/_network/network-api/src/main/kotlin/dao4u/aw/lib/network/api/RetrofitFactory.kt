package dao4u.aw.lib.network.api

interface RetrofitApiFactory {
    fun <T> createApi(api: Class<T>): T
}